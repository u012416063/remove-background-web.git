---
title: Remove Background WebGPU
emoji: 🖼️
colorFrom: yellow
colorTo: red
sdk: static
pinned: false
models:
- briaai/RMBG-1.4
short_description: In-browser WebGPU background removal
---

Check out the configuration reference at https://huggingface.co/docs/hub/spaces-config-reference

详细介绍可以查看这篇博文: https://blog.csdn.net/u012416063/article/details/143238770